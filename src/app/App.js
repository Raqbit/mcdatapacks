import React, {Component} from 'react';
import Main from './containers/Main';
import Footer from './components/Footer';
import Navbar from './components/Navbar';

class App extends Component {
    render() {
        return (
            <div>
                <Navbar/>
                <Main/>
                <Footer/>
            </div>
        );
    }
}

export default App;
