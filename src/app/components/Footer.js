import React from 'react';

const Footer = () => (
    <footer>Copyright &copy; {new Date().getFullYear()} MCDatapacks</footer>
);

export default Footer;