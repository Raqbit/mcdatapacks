import React from 'react';

const Navbar = () => (
    <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky">
        <nav className="uk-navbar-container" uk-navbar="true">
            <div className="uk-navbar-left">
                <ul className="uk-navbar-nav">
                    <li className="uk-active"><a href="/">Home</a></li>
                    <li className="uk-parent"><a href="/">Link2</a></li>
                    <li><a href="/">Stuff</a></li>
                </ul>
            </div>
        </nav>
    </div>
);

export default Navbar;