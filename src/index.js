import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import App from './app/App';
import registerServiceWorker from './registerServiceWorker';
import {createStore} from 'redux';
import {devToolsEnhancer} from 'redux-devtools-extension';
import {BrowserRouter as Router} from 'react-router-dom';
import rootReducer from './rootReducer';
import 'uikit/dist/css/uikit.css'
import 'uikit/dist/js/uikit'
import './index.css'

const store = createStore(rootReducer, devToolsEnhancer());

const publicUrl = process.env.PUBLIC_URL; // For running app under sub-path
const basename = publicUrl ? new URL(publicUrl).pathname : '/';

const Root = () =>
    (
        <Provider store={store}>
                <Router basename={basename}>
                    <App/>
                </Router>
        </Provider>
    );

ReactDOM.render(<Root />, document.getElementById('root'));
registerServiceWorker();
