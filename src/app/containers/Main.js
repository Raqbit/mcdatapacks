import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from '../pages/Home';

class Main extends Component {
    render() {
        return (
            <main> {/* This is the native 'main' html element */}
                <Switch>
                    <Route exact path="/" component={Home}/>
                </Switch>
            </main>
        );
    }
}

export default Main;
